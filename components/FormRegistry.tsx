import React from "react";
import { useForm, SubmitHandler } from "react-hook-form";


interface Props {
  name? :string
}

type InputsForm = {
  name: string,
  author: string,
};

const FormRegistry: React.FC<Props> = (props: Props) => {
  const { register, handleSubmit, watch, formState: { errors } } = useForm<InputsForm>();
  const onSubmit: SubmitHandler<InputsForm> = data => console.log(data);

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <input {...register("name", { required: true })} />
        {errors.author && <span>This field is required</span>}

        <br />
        
        <input {...register("author", { required: true })} />
        {errors.author && <span>This field is required</span>}

        <br />
        
        <input type="submit" />
      </form>
    </>
  )
}

export default FormRegistry;
