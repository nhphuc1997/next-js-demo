import Link from "next/link";
import FormRegistry from "../components/FormRegistry";

const Home = () => {
  return (
   <>
    <div>
      <p>Index pages</p>
    </div>
    <div>
      React hook form
      <FormRegistry />
    </div>
    <div>
      <Link href={'/document'}>Link to document</Link>
    </div>
   </>
  )
}

export default Home;
