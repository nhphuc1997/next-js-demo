import { useQuery } from "@tanstack/react-query";
import { useRouter } from "next/router";
import axiosInstance from "../../axiosInstance";

const DetailDocument = () => {
  const router = useRouter();
  const { slug } = router.query;

  const {isLoading, error, data} = useQuery([], async () => {
    return await axiosInstance.get(`/document/${slug}`)
  })

  if(isLoading) return <>Loading....</>
  if (error) return 'An error has occurred: ' + error
  
  return (
    <>
      <div>
        <p>Detail document pages:</p>
        {data && (
          <>
            <div>
              <p>Id: {data.data.id}</p>
              <p>Name: {data.data.name}</p>
              <p>Url: {data.data.avatar}</p>
              <p>createdAt: {data.data.createdAt}</p>
            </div>
          </>
        )}
      </div>
    </>
  )
}

export default DetailDocument;
