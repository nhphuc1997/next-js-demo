import { useQuery } from "@tanstack/react-query";
import Link from "next/link";
import { useState } from "react";
import axiosInstance from "../../axiosInstance";

interface IDocument {
  id: number,
  name: string,
  author: string,
}

const Document = () => {
  const {isLoading, error, data} = useQuery([`document`], async () => {
    return await axiosInstance.get(`/document`);
  })

  if(isLoading) return <>Loading.....</>
  if (error) return 'An error has occurred: ' + error

  return (
    <>
     <div>
       <p>Dcoument pages</p>
     </div>
     <div>
      <ul>
      {data && data.data.map((document:IDocument) => (
        <li>
          <Link href={{ pathname: '/document/[slug]', query: { slug: document.id } }}>
            {document.name}
          </Link>
        </li>
      ))}
      </ul>
     </div>
    </>
   )
}

export default Document;
