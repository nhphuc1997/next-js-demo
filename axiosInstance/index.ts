import axios from "axios";

const axiosInstance = axios.create({
    timeout: 1000,
    headers: {'X-Custom-Header': 'foobar'},
    baseURL: 'https://63a3c8879704d18da095bdbc.mockapi.io/api/v1'
  });

export default axiosInstance;
